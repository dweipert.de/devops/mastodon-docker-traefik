Mastodon Docker Setup with traefik
===

The scripts provided modify the official `docker-compose.yml` file
to be

- fast and easy to set up, without build step
- usable with [traefik](https://traefik.io/)

The provided [docker-compose.yml](docker-compose.yml) file can be used to set up traefik.

## Requirements

- PHP
- Docker
- [traefik](https://traefik.io/)


## Instructions

Run the scripts in the following order

```
./bin/build.php
./bin/setup.php
./bin/run.php
```

### Build

```
./bin/build.php
```

Builds the docker-compose.yml file with traefik labels


### Setup

```
./bin/setup.php
```

This runs the `mastodon:setup` command to populate the `.env.production` file.

Config:

- Domain name: ${same as in .env DOMAIN}
- Single user mode?: up to you
- Using docker?: Yes
- PostgreSQL host: default (db)
- PostgreSQL port: default (5432)
- PostgreSQL database: default (postgres)
- PostgreSQL user: default (postgres)
- PostgreSQL password: ${same as in .env POSTGRES_PASSWORD}
- Redis host: default (redis)
- Redis port: default (6379)
- Redis password: default (empty)
- Store uploaded files on the cloud?: up to you
- Send e-mails from localhost?: up to you
- Save configuration?: y
- Prepare the database now?: y
- Create admin user?: up to you


### Run

```
./bin/run.php
```

Runs mastodon


## Using only the built docker-compose.yml file

You can get the most recent build here [docker-compose.yml](https://gitlab.com/dweipert.de/devops/mastodon-docker-traefik/-/packages)


### First time setup

- get the [.env.example](.env.example) file and save it as `.env`

- create `.env.production`
```
touch .env.production
```

- run `bundle exec rake mastodon:setup` in the `web` container
```
docker-compose run --rm -v $PWD/.env.production:/opt/mastodon/.env.production web bundle exec rake mastodon:setup
```


### Run all services

```
docker-compose up -d
```


## Credits

- https://github.com/peterrus/docker-mastodon
- https://gist.github.com/smashnet/38cf7c30cb06427bab78ae5ab0fd2ae3
- https://www.innoq.com/en/blog/traefik-v2-and-mastodon/
- https://gist.github.com/fredix/6ef5c95c600dbbb9afc0e0538a93d90d
- https://www.howtoforge.com/how-to-install-mastodon-social-network-with-docker-on-ubuntu-1804/
- https://github.com/silkkycloud/bitwarden
