#!/usr/bin/env php

<?php

$rootDir = dirname(__DIR__);
$buildDir = "$rootDir/build";

// check if .env is propery set
if (! file_exists("$buildDir/.env")) {
    copy("$rootDir/.env.example", "$buildDir/.env");
    exit('Adjust .env in build directory first.');
}
else if (strpos(file_get_contents("$buildDir/.env"), 'example.org') !== false) {
    exit('Adjust .env in build directory first.');
}

// load .env
$dotenv = \Dotenv\Dotenv::createImmutable($buildDir);
$dotenv->load();

// change to build dir
chdir($buildDir);

// create .env.production for mastodon setup
exec("touch $buildDir/.env.production");

// run mastodon:setup
exec("docker-compose run --rm -v $buildDir/.env.production:/opt/mastodon/.env.production web bundle exec rake mastodon:setup");
