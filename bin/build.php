#!/usr/bin/env php

<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

$rootDir = dirname(__DIR__);
$buildDir = "$rootDir/build";
@mkdir($buildDir);

// get official compose file
$dockerCompose = Yaml::parse(file_get_contents('https://raw.githubusercontent.com/tootsuite/mastodon/main/docker-compose.yml'));

// set missing POSTGRES_PASSWORD
$dockerCompose['services']['db']['environment'][] = 'POSTGRES_PASSWORD=${POSTGRES_PASSWORD}';

// set traefik labels for external services
$dockerCompose['services']['web']['labels'] = [
    'traefik.enable=true',
    'traefik.docker.network=${TRAEFIK_NETWORK}',
    'traefik.http.services.${COMPOSE_PROJECT_NAME}-web.loadbalancer.server.port=3000',
    'traefik.http.routers.${COMPOSE_PROJECT_NAME}-web.rule=Host(`${DOMAIN}`)',
    'traefik.http.routers.${COMPOSE_PROJECT_NAME}-web.entrypoints=websecure',
    'traefik.http.routers.${COMPOSE_PROJECT_NAME}-web.tls.certresolver=letsencrypt',
];
$dockerCompose['services']['streaming']['labels'] = [
    'traefik.enable=true',
    'traefik.docker.network=${TRAEFIK_NETWORK}',
    'traefik.http.services.${COMPOSE_PROJECT_NAME}-streaming.loadbalancer.server.port=4000',
    'traefik.http.routers.${COMPOSE_PROJECT_NAME}-streaming.rule=(Host(`${DOMAIN}`) && PathPrefix(`/api/v1/streaming`))',
    'traefik.http.routers.${COMPOSE_PROJECT_NAME}-streaming.entrypoints=websecure',
    'traefik.http.routers.${COMPOSE_PROJECT_NAME}-streaming.tls.certresolver=letsencrypt',
];

// remove build instructions to use pre-built images
unset($dockerCompose['services']['web']['build']);
unset($dockerCompose['services']['streaming']['build']);
unset($dockerCompose['services']['sidekiq']['build']);

// set external network for use with traefik
$dockerCompose['services']['web']['networks'][] = '${TRAEFIK_NETWORK}';
$dockerCompose['services']['streaming']['networks'][] = '${TRAEFIK_NETWORK}';
$dockerCompose['networks']['${TRAEFIK_NETWORK}']['external'] = true;

// write to build dir
file_put_contents("$buildDir/docker-compose.yml", Yaml::dump($dockerCompose, 99));
